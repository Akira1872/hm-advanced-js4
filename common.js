fetch('https://ajax.test-danit.com/api/swapi/films')
.then(response => {
    if(!response.ok) throw new Error ('Error not found');
    return response.json()
}) 
.then (data => {
   data.forEach(elements => {
    const {characters, episodeId, name, openingCrawl} = elements;
    const ul = document.createElement('ul');
    const li = document.createElement('li');
    li.textContent = (`Name: ${name}, Episode: ${episodeId}, Description: ${openingCrawl}`);
    ul.append(li);
    characters.forEach(charactersName => {
        fetch(charactersName)
        .then(response => {
            if(!response.ok) throw new Error ('Error not found');
            return response.json()
        })
        .then(characters => {
            const {birthYear, eyeColor, gender, hairColor, height, name} = characters;
            const liCharacter = document.createElement('li');
            liCharacter.textContent = (`Name: ${name}, Birth Year: ${birthYear}, Gender: ${gender}, Color eye: ${eyeColor}, HairColor: ${hairColor}, Height: ${height}`);
            li.append(liCharacter); 
            
        })
        .catch (err =>{
            console.error('Error:', err);
        }); 
    })
    document.body.appendChild(ul);
    })
   })
.catch (err =>{
    console.error('Error:', err);
});

 